package com.ascendcorp.exam.service;

import com.ascendcorp.exam.constant.InquiryStausCode;
import com.ascendcorp.exam.model.InquiryDTO;
import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.model.TransferResponse;
import com.ascendcorp.exam.proxy.BankProxyGateway;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.xml.ws.WebServiceException;

@Slf4j
@RequiredArgsConstructor
@Service
public class InquiryService {


    private final BankProxyGateway bankProxyGateway;

    private final ReponseService reponseService;

    public InquiryServiceResultDTO inquiry(InquiryDTO inquiry) {

        InquiryServiceResultDTO respDTO = null;
        try {

            log.info("call bank web service");
            TransferResponse response = bankProxyGateway.requestTransfer(inquiry.getTransactionId(), inquiry.getTranDateTime(), inquiry.getChannel(),
                    inquiry.getBankCode(), inquiry.getBankNumber(), inquiry.getAmount(), inquiry.getReference1(), inquiry.getReference2());

            log.info("check bank response code");
            respDTO = reponseService.checkResponse(response,respDTO);

        } catch (NullPointerException ne) {
            if (respDTO == null) {
                respDTO = new InquiryServiceResultDTO();
                respDTO.setReasonCode(InquiryStausCode.TRANSACTION_ERROR_500.getCode());
                respDTO.setReasonDesc(InquiryStausCode.TRANSACTION_ERROR_500.getDesc());
            }
        } catch (WebServiceException r) {
            // handle error from bank web service
            String faultString = r.getMessage();
            if (respDTO == null) {
                respDTO = new InquiryServiceResultDTO();
                if (faultString != null && (faultString.indexOf("java.net.SocketTimeoutException") > -1
                        || faultString.indexOf("Connection timed out") > -1)) {
                    // bank timeout
                    respDTO.setReasonCode(InquiryStausCode.TIME_OUT_503.getCode());
                    respDTO.setReasonDesc(InquiryStausCode.TIME_OUT_503.getDesc());
                } else {
                    // bank general error
                    respDTO.setReasonCode(InquiryStausCode.APP_ERROR_504.getCode());
                    respDTO.setReasonDesc(InquiryStausCode.APP_ERROR_504.getDesc());
                }
            }
        } catch (Exception e) {
            log.error("inquiry exception", e);
            if (respDTO == null || (respDTO != null && respDTO.getReasonCode() == null)) {
                respDTO = new InquiryServiceResultDTO();
                respDTO.setReasonCode(InquiryStausCode.APP_ERROR_504.getCode());
                respDTO.setReasonDesc(InquiryStausCode.APP_ERROR_504.getDesc());
            }
        }
        return respDTO;
    }
}
