package com.ascendcorp.exam.service;

import com.ascendcorp.exam.constant.InquiryStausCode;
import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.model.TransferResponse;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class ReponseService {

    final static Logger log = Logger.getLogger(ReponseService.class);


    public InquiryServiceResultDTO checkResponse(TransferResponse response, InquiryServiceResultDTO respDTO) throws Exception {

        if (response != null) //New
        {
            log.debug("found response code");
            respDTO = new InquiryServiceResultDTO();

            respDTO.setRef_no1(response.getReferenceCode1());
            respDTO.setRef_no2(response.getReferenceCode2());
            respDTO.setAmount(response.getBalance());
            respDTO.setTranID(response.getBankTransactionID());

            if (response.getResponseCode().equalsIgnoreCase("approved")) {
                // bank response code = approved
                respDTO.setReasonCode(InquiryStausCode.APPROVED_200.getCode());
                respDTO.setReasonDesc(response.getDescription());
                respDTO.setAccountName(response.getDescription());

            } else if (response.getResponseCode().equalsIgnoreCase("invalid_data")) {
                // bank response code = invalid_data
                String replyDesc = response.getDescription();
                if (replyDesc != null) {
                    String respDesc[] = replyDesc.split(":");
                    if (respDesc != null && respDesc.length >= 3) {
                        // bank description full format
                        respDTO.setReasonCode(respDesc[1]);
                        respDTO.setReasonDesc(respDesc[2]);
                    } else {
                        // bank description short format
                        respDTO.setReasonCode(InquiryStausCode.INVALID_DATA_400.getCode());
                        respDTO.setReasonDesc(InquiryStausCode.INVALID_DATA_400.getDesc());
                    }
                } else {
                    // bank no description
                    respDTO.setReasonCode(InquiryStausCode.INVALID_DATA_400.getCode());
                    respDTO.setReasonDesc(InquiryStausCode.INVALID_DATA_400.getDesc());
                }

            } else if (response.getResponseCode().equalsIgnoreCase("transaction_error")) {
                // bank response code = transaction_error
                String replyDesc = response.getDescription();
                if (replyDesc != null) {
                    String respDesc[] = replyDesc.split(":");
                    if (respDesc != null && respDesc.length >= 2) {
                        log.info("Case Inquiry Error Code Format Now Will Get From [0] and [1] first");
                        String subIdx1 = respDesc[0];
                        String subIdx2 = respDesc[1];
                        log.info("index[0] : " + subIdx1 + " index[1] is >> " + subIdx2);
                        if ("98".equalsIgnoreCase(subIdx1)) {
                            // bank code 98
                            respDTO.setReasonCode(subIdx1);
                            respDTO.setReasonDesc(subIdx2);
                        } else {
                            log.info("case error is not 98 code");
                            if (respDesc.length >= 3) {
                                // bank description full format
                                String subIdx3 = respDesc[2];
                                log.info("index[0] : " + subIdx3);
                                respDTO.setReasonCode(subIdx2);
                                respDTO.setReasonDesc(subIdx3);
                            } else {
                                // bank description short format
                                respDTO.setReasonCode(subIdx1);
                                respDTO.setReasonDesc(subIdx2);
                            }
                        }
                    } else {
                        // bank description incorrect format
                        respDTO.setReasonCode(InquiryStausCode.TRANSACTION_ERROR_500.getCode());
                        respDTO.setReasonDesc(InquiryStausCode.TRANSACTION_ERROR_500.getDesc());
                    }
                } else {
                    // bank no description
                    respDTO.setReasonCode(InquiryStausCode.TRANSACTION_ERROR_500.getCode());
                    respDTO.setReasonDesc(InquiryStausCode.TRANSACTION_ERROR_500.getDesc());
                }
            } else if (response.getResponseCode().equalsIgnoreCase("unknown")) {
                String replyDesc = response.getDescription();
                if (replyDesc != null) {
                    String respDesc[] = replyDesc.split(":");
                    if (respDesc != null && respDesc.length >= 2) {
                        // bank description full format
                        respDTO.setReasonCode(respDesc[0]);
                        respDTO.setReasonDesc(respDesc[1]);
                        if (respDTO.getReasonDesc() == null || respDTO.getReasonDesc().trim().length() == 0) {
                            respDTO.setReasonDesc(InquiryStausCode.INVALID_DATA_501.getDesc());
                        }
                    } else {
                        // bank description short format
                        respDTO.setReasonCode(InquiryStausCode.INVALID_DATA_501.getCode());
                        respDTO.setReasonDesc(InquiryStausCode.INVALID_DATA_501.getDesc());
                    }
                } else {
                    // bank no description
                    respDTO.setReasonCode(InquiryStausCode.INVALID_DATA_501.getCode());
                    respDTO.setReasonDesc(InquiryStausCode.INVALID_DATA_501.getDesc());
                }
            } else
                // bank code not support
                throw new Exception("Unsupport Error Reason Code");
        } else {
            // no resport from bank
            throw new Exception("Unable to inquiry from service.");
        }
        return respDTO;
    }
}
