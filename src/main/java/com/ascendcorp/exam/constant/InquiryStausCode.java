package com.ascendcorp.exam.constant;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum InquiryStausCode {

    APPROVED_200("200", "approved"),
    INVALID_DATA_400("400", "General Invalid Data"),
    TRANSACTION_ERROR_500("500","General Transaction Error"),
    INVALID_DATA_501("501","General Invalid Data"),
    TIME_OUT_503("503","Error timeout"),
    APP_ERROR_504("504","Internal Application Error");




    private final String code;
    private final String desc;


}
