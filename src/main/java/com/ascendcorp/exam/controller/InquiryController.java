package com.ascendcorp.exam.controller;

import com.ascendcorp.exam.model.InquiryDTO;
import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.service.InquiryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class InquiryController {

    private final InquiryService inquiryService;

    @GetMapping("/inquiry")
    public ResponseEntity<InquiryServiceResultDTO> inquiry(@Validated @RequestBody InquiryDTO request){

        InquiryServiceResultDTO response = inquiryService.inquiry(request);

        return ResponseEntity.ok(response);
    }
}
