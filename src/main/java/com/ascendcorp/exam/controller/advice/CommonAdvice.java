package com.ascendcorp.exam.controller.advice;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestControllerAdvice
public class CommonAdvice extends ResponseEntityExceptionHandler {

    @Data
    private class ExceptionResponse {

        private String code;
        private String message;
        private LocalDateTime timestamp;

    }

    private ResponseEntity<Object> handle (String message, HttpStatus status, String code) {
        ExceptionResponse response = new ExceptionResponse();
        response.setCode("xxx-"+code);
        response.setMessage(message);
        response.setTimestamp(LocalDateTime.now());
        return ResponseEntity.status(status).body(response);
    }

    private ResponseEntity<Object> validateHandle (List<FieldError> fieldErrorList, HttpStatus status, String code) {
        List<ExceptionResponse> responseList = new ArrayList<>();
        for (FieldError field:fieldErrorList) {
            ExceptionResponse response = new ExceptionResponse();
            response.setCode("xxx-" + code);
            if (field.getField().equals("amount")){
                log.info(field.getField() + " must more than zero!");
                response.setMessage(field.getField()+ " must more than zero!");
            } else {
                log.info(field.getField() + " is required!");
                response.setMessage(field.getField()+ " is required!");
            }
            response.setTimestamp(LocalDateTime.now());
            responseList.add(response);
        }

        return ResponseEntity.status(status).body(responseList);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handle(ex.getMessage(), status, String.valueOf(status.value()));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return validateHandle(ex.getBindingResult().getFieldErrors(),status,String.valueOf(status.value()));
    }
}
