package com.ascendcorp.exam.model;

import lombok.Data;

@Data
public class InquiryServiceResultDTO {

    private String tranID;

    private String namespace;

    private String reasonCode;

    private String reasonDesc;

    private String balance;

    private String ref_no1;

    private String ref_no2;

    private String amount;

    private String accountName;


}
