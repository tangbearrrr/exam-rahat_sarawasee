package com.ascendcorp.exam.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class InquiryDTO {

    @NotNull
    String transactionId;
    @NotNull
    LocalDateTime tranDateTime;
    @NotNull
    String channel;
    String locationCode;
    @NotNull
    String bankCode;
    @NotNull
    String bankNumber;

    @DecimalMin("1")
    double amount;
    String reference1;
    String reference2;
    String firstName;
    String lastName;
}
